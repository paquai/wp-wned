#!/bin/bash
# conf
wp_wned_path="$(dirname $0)"
if [ ! -f $wp_wned_path/ignores.txt ]; then
    echo "^$" > $wp_wned_path/ignores.txt
fi

# defaults to a search restricted to files from wordpress sites changed durang last 24 hours, being actually inside /home/*/www/ and not being slave sites
FIND_ARGS=""
LOCATIONS="$1"
if [[ ( "$1" == "" ) ]];
then
    LOCATIONS="$(locate '*/wp-content/plugins' | grep '/home/[^/]*/www/' | grep 'wp-content.plugins$' | sed 's/..................$//g' | sort | uniq)"
    FIND_ARGS="$FIND_ARGS -ctime -1"
fi
# do not search in slave sites
SLAVES=$(echo "$LOCATIONS" | xargs -I % grep -Hsl 'STATE=slave' "%../state" | sed 's/..\/state$//g')
SLAVES_PATTERN=$(echo $SLAVES | sed 's/ /\\|/g')
MASTER_LOCATIONS="$LOCATIONS"
if [[ ( "$SLAVES_PATTERN" != "" ) ]];
then
    MASTER_LOCATIONS=$(echo "$LOCATIONS" | grep -vx "$SLAVES_PATTERN")
fi
PHP_FILES_LIST=$(echo "$MASTER_LOCATIONS" | xargs -I % find "%" -type f \( -iname "*.php" -or -iname "*.inc" -or -iname "*.module" -or -iname "*.phtml" \) -size +0 $FIND_ARGS)

# redirects stderr to stdin before we go
exec 2>&1

# look for php files in wp-content/uploads (if it exists)
echo "$PHP_FILES_LIST" | grep "wp-content/uploads/" | grep -v -f $wp_wned_path/ignores.txt;

# look for obvious malicious files (various evals, obfuscated code, strange characters...)
REGEXP_KNOWN_HACKS='_YM82iAN\|$compressed=base64_decode($cookieData);\|r57shell.php\|c999sh_'
REGEXP_INJECTS_1='\(^\|[^a-z]\)eval *(.*\(decode\|inflate\|rot13\|qrpbqr\|vasyngr\) *(.*)'
REGEXP_INJECTS_2='\(decode\|inflate\|rot13\|qrpbqr\|vasyngr\) *(.*eval *(.*)'
REGEXP_OBFUSCATED='^\s*\$[a-z0-9_]\+=.\{40,\}[^-]\.$'
REGEXP_STRANGE_CHARS_1='\(\\x[0-9a-z]\{2\}\)\{8,\}'
REGEXP_STRANGE_CHARS_2='\(\\[0-9]\{3\}\)\{8,\}'
echo "$PHP_FILES_LIST" \
    | xargs -I % grep -Haior "\($REGEXP_KNOWN_HACKS\)\|\($REGEXP_INJECTS_1\)\|\($REGEXP_INJECTS_2\)\|\($REGEXP_OBFUSCATED\)\|\($REGEXP_STRANGE_CHARS_1\)\|\($REGEXP_STRANGE_CHARS_2\)" "%" 2>&1 \
    | grep -v ': No such file or directory$' \
    | grep -o '^.*\.php:.\{1,255\}' \
    | grep -v -f $wp_wned_path/ignores.txt;

# look for PHP files containing very long strings
REGEXP_TOO_LONG='[^ :]\{255\}[^ :]\{255\}[^ :]\{255\}'
echo "$PHP_FILES_LIST" | xargs -I % sh -c 'EXCERPT=$(cut -c 768-1534 "%" | grep -v "^$"); echo "%:$EXCERPT"; ' 2>&1 \
    | grep -v ': No such file or directory$' \
    | grep -v '\.php:$' \
    | grep "$REGEXP_TOO_LONG" \
    | grep -o '^.*\.php:.\{1,255\}' \
    | grep -v -f $wp_wned_path/ignores.txt;

# look for binary code inside PHP files
echo "$PHP_FILES_LIST" | xargs -I % grep -alP '[^[:print:]\t\r[\x80-\xbf]]' "%" 2>&1 \
    | grep -v ': No such file or directory$' \
    | grep -v -f $wp_wned_path/ignores.txt;

# ignore return codes
exit 0

# TODO: look for improvements in similar projects : PHP antivirus, sucuri, wordfence, Joomla-Anti-Malware-Scan-Script...
